import tkinter as tk

from .ContactList import ContactList
from .tabContainer import TabContainer, Tab


class ChatWindow(tk.Frame):

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.build()
    

    def build(self):
        self.contacts = ContactList(self, name='contacts')
        self.contacts.pack(side='left', fill='y', expand=True)
        self.tabs = TabContainer(self, name='tabs')
        self.tabs.pack(side='left', fill='both', expand=True)

        self.contacts.bind('<Double-Button-1>', self.onContactDoubleClick)

    def onContactDoubleClick(self, event):
        contacts = event.widget
        item_ids = contacts.curselection()
        username = contacts.get(item_ids[0])
        
        self.tabs.add(Tab(self.tabs, controller=self.parent, name=username.lower()), text=username)