import tkinter as tk

class ContactList(tk.Listbox):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def update(self, contacts):
        self.clear()
        for contact in contacts:
            self.insert('end', contact)

    def clear(self):
        self.delete(0, 'end')

    def populate(self, contacts):
        self.clear()
        self.update(contacts)