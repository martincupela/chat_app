import tkinter as tk
from tkinter import ttk


class TabContainer(ttk.Notebook):
    
    def __init__(self, parent, *args, **kwargs):
        self.init_style()
        super().__init__(parent, style="ClosableTab", *args, **kwargs)
        self.parent = parent

        self.bind('<ButtonPress-1>', self.on_close_press, True)
        self.bind('<ButtonRelease-1>', self.on_close_release)

    def init_style(self):
        s = ttk.Style()
        self.create_close_element(s)
        self.create_layout(s)

    def get_active_tab_name(self):
        full_name = self.select()
        return full_name.split('.')[-1]

    def create_close_element(self, style):
        image_data ='''
                R0lGODlhCAAIAMIEAAAAAOUqKv9mZtnZ2Ts7Ozs7Ozs7Ozs7OyH+EUNyZWF0ZWQg
                d2l0aCBHSU1QACH5BAEKAAQALAAAAAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU
                5kEJADs=
                '''
        default_image_name = 'img_close'

        self.images = (tk.PhotoImage(default_image_name, data= image_data), )

        name = 'close'
        el_type = 'image'
        styles = {"border":8, "sticky":'w'}

        style.element_create(name, el_type, default_image_name, **styles)

    def create_layout(self, style):
        style.layout('ClosableTab', [('ClosableTab.client', {'sticky': 'nsew'})])

        style.layout('ClosableTab.Tab',[
            ('ClosableTab.tab',{
                'children': [
                    ('ClosableTab.padding',
                                 {'children': [
                                    ('ClosableTab.focus',
                                        {'children': [
                                            ('ClosableTab.label',
                                               {'side': 'left', 
                                               'sticky': 'w'}),
                                            ('ClosableTab.close',
                                                {'side': 'right', 
                                               'sticky': 'e',})],
                                     'side': 'top',
                                     'sticky': 'nswe'})],
                              'side': 'top',
                              'sticky': 'nswe'})],
               'sticky': 'nswe'})]
            )

    def on_close_press(self, event):
        
        elem_name = self.identify(event.x, event.y)

        if 'close' in elem_name:
            tab_index = self.index("@%d,%d" % (event.x, event.y))
            self.state(['pressed'])
            self.active_tab = tab_index

    def on_close_release(self, event):

        if not self.instate(['pressed']):
            return

        elem_name =  self.identify(event.x, event.y)
        tab_index = self.index("@%d,%d" % (event.x, event.y))

        if "close" in elem_name and self.active_tab == tab_index:
            self.forget(tab_index)

        self.state(["!pressed"])
        self.active_tab = None