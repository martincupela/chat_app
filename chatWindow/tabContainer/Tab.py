from tkinter import ttk

from .MessagePane import MessagePane
from .MessageEntry import MessageEntry

class Tab(ttk.Notebook):
    
    def __init__(self, parent, controller, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.parent = parent
        self.controller = controller
        self.build()
    

    def build(self):
        messages = MessagePane(self, name='messages')
        msg_input = MessageEntry(self, controller=self.controller, name='msg_input')
        messages.pack(side='top', fill='x')
        msg_input.pack(side='bottom', fill='x')

        messages.config(state='disabled')