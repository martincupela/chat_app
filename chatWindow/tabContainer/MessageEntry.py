import tkinter as tk

class SubmitButton(tk.Button):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class MessageEntry(tk.Text):
    
    def __init__(self, parent, controller, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.controller = controller
        self.build()

    def build(self):
        button = SubmitButton(self, text="Send", name="button", command=self.controller.send_message)
        button.place(relx=1, x=-5, y=5, anchor='ne')

    def get_content(self):
        return self.get('1.0', tk.END)

    def clear(self):
        self.delete(1.0, 'end')