import tkinter as tk

class MessagePane(tk.Text):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def put_message(self, msg, timestamp, sender):
        header = '{} | {}\n'.format(sender, timestamp)
        self.config(state='normal')
        self.insert("end", header)
        self.insert("end", msg + '\n')
        self.config(state='disabled')