import socket


class Server:
    '''Simple TCP server'''

    SOCKET_FAMILY = socket.AF_INET
    SOCKET_TYPE = socket.SOCK_STREAM

    def __init__(self, host, port):
        '''
        :param host: host IP address
        :type host: string
        :param port: port number to bind to
        :type port: int
        '''
        self.address = (host, port)
        self._listener = socket.socket(self.SOCKET_FAMILY, self.SOCKET_TYPE)
        
        self.bind()
        self.activate()

    def bind(self):
        '''Take the port number at given host'''
        self._listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._listener.bind(self.address)

    def activate(self):
        '''Used by TCP server to listen at a port'''
        self._listener.listen()
        print('Running service at {}:{}.'.format(*self.address))

    def serve(self):
        '''Contains the main serving loop'''
        try:
            while True:
                self.handle()
        finally:
            self.shutdown()
            exit()

    def recvall(self, sock):
        pass

    def shutdown(self):
        '''Close the server socket'''
        if self._listener._closed:
            print('Server already shut down')
        else:
            self._listener.close()
            print('The server at {} has been shutdown'.format(self.address))