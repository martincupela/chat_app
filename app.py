import argparse
import json
import tkinter as tk
import threading

from chatWindow import ChatWindow
from loginWindow import LoginWindow
from client import Client

PAGES = (LoginWindow, ChatWindow)

class App(tk.Tk):

    HEIGHT = 500
    WIDTH = 800

    def __init__(self, host, port, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pages = {}
        self.build()
        self.center()
        self.title('Chat Client')
        self.show_frame('LoginWindow')
        self.host = host
        self.port = port

    def build(self):
        self.frames = {}
        for Page in PAGES:
            page = Page(self)
            page.grid(row=0, sticky="nsew")
            self.frames.update({Page.__name__: page})

    def center(self):

        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        x = (screen_width - self.WIDTH) // 2
        y = (screen_height - self.HEIGHT) // 2

        dimensions = {
            'width': self.WIDTH, 
            'height': self.HEIGHT, 
            'offset_x': x, 
            'offset_y' :y
        }

        self.geometry('{width}x{height}+{offset_x}+{offset_y}'.format(**dimensions))

    def show_frame(self, frame_page):
        frame = self.frames[frame_page]
        frame.lift()

    def init_connect(self):
        self.socket_lock = threading.Lock()
        self.client = Client(
            self.host, 
            self.port,
            self.socket_lock 
        )

        try:
            self.frames['LoginWindow'].progress_info.configure(text='Connecting to the server')
            
            self.client.connect()

            self.client.username = self.frames['LoginWindow'].login_form.username_entry.get()

            result = self.client.login()

            if result == 'username taken':
                self.frames['LoginWindow'].progress_info.configure(text='Username taken, please enter a new one')
                
            elif result.startswith('success'):
                self.title(f'Hi {self.client.username}')
                self.frames['LoginWindow'].progress_info.configure(text='Success...')
                users = json.loads(result.strip('success'))
                self.frames['ChatWindow'].contacts.populate(users)
                self.show_frame('ChatWindow')

                self.start_receiver()

        except ConnectionRefusedError:
            self.frames['LoginWindow'].progress_info.configure(text='The service is down...')
            self.client.shutdown()

        except ConnectionError:
            self.frames['LoginWindow'].progress_info.configure(text='The connection has been interrupted')
            self.client.shutdown()

    def start_receiver(self):
        self.receiver_thread = threading.Thread(
            target=self.receive, 
            daemon=True)
        self.receiver_thread.start()

    def receive(self):
        '''Take care of receiving the messages'''
        try:
            while True:
                messages = self.client.get_message()
                self.publish(messages)
        finally:
            self.client.shutdown()

    def send_message(self):
        active_name = self.frames['ChatWindow'].tabs.get_active_tab_name()
        msg_input = self.frames['ChatWindow'].tabs.children[active_name].children['msg_input']
        text = msg_input.get_content()

        request = self.client.create_message(msg=text, to=active_name)
        self.socket_lock.acquire(timeout=0.3)
        self.client.socket.sendall(request)

        messages = self.client.parse_messages(request)
        
        self.publish(messages, sent=True)

        msg_input.clear()


    def publish(self, messages,*, sent=False):
    
        for message in sorted(messages, key=lambda message: message['header']['sent']):
            sender = message['header']['from']
            
            if sender == 'server' and message['body'].startswith('users'):
                users = json.loads(message['body'].strip('users'))
                
                self.frames['ChatWindow'].contacts.populate(users)
            
            elif sent or sender != self.client.username:
                timestamp = message['header']['sent']
                target_tab =  message['header']['to'] if sent else message['header']['from']

                message_pane = self.frames['ChatWindow'].tabs.children[target_tab].children['messages']

                sender = 'YOU' if sent else sender
                
                message_pane.put_message(message['body'], timestamp, sender)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Simple TCP client args')
    parser.add_argument('--host', default="localhost", 
                        help='interface the server listens at (IP:PORT);')
    parser.add_argument('--port', metavar='PORT', type=int, default=9000,
                        help='TCP port (default 9000)')

    args = parser.parse_args()
    try:
        app = App(**vars(args))
        app.mainloop()
    finally:
        app.client.shutdown()