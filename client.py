import socket
import argparse

from mixin import MessageProcessingMixin


class Client(MessageProcessingMixin):

    SOCKET_FAMILY = socket.AF_INET
    SOCKET_TYPE = socket.SOCK_STREAM

    def __init__(self, host, port, socket_lock, username=None):
        '''
        :param host: host IP address
        :type host: string
        :param port: port number to bind to
        :type port: int
        '''
        self.server_address = (host, port)
        self.socket = socket.socket(self.SOCKET_FAMILY, self.SOCKET_TYPE)
        self.username = username
        self.socket_lock = socket_lock


    def login(self):
        request = self.create_message(msg='login')
        self.socket.sendall(request)

        received = self.recvall(self.socket)
        if received:
            msg = self.parse_messages(received)[0]
            return msg['body']
        else:
            raise ConnectionRefusedError

    def connect(self):
        '''Connect to the server socket'''
        self.socket.connect(self.server_address)
        self.socket.settimeout(2)

    def shutdown(self):
        '''Close the client socket'''
        print('\nShutting down the connection...')
        self.socket.close()

    def run(self):
        '''Provide user interface for sending messages'''
        try:
            while True:
                self.make_message()
                self.get_message()
        finally:
            self.shutdown()

    def make_message(self):
        '''
        Collect user inputs and send it to the server
        '''
        to, data = "", ""
        while not any([to, data]):
            to = input('TO: ')
            data = input('YOUR MESSAGE: ')
            if not data:
                print('Please enter your message')
            if not to:
                print('Please enter the target username')
        request = self.create_message(msg=data, to=to)
        self.socket.sendall(request)

    def get_message(self):
        self.socket_lock.acquire(timeout=0.3)           
        received = self.recvall(self.socket)

        if received:
            return self.parse_messages(received)
        return ''

    def recvall(self, client, chunk_size=16):
        data = b''

        try:
            while True:
                received = client.recv(chunk_size)
                if not received: # connection terminated
                    raise ConnectionError
                else:
                    data += received
        except (BlockingIOError, socket.timeout):
            print('Finished reading...')
            return data
