import tkinter as tk
from .LoginForm import LoginForm 


class LoginWindow(tk.Frame):

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.build()
    

    def build(self):
        self.progress_info = tk.Message(self, width=200)
        self.login_form = LoginForm(self, controller=self.parent, name="login_form")
        self.progress_info.pack()
        self.login_form.pack()