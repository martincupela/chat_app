import tkinter as tk


class LoginForm(tk.Frame):

    def __init__(self, parent, controller, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.parent = parent
        self.controller = controller
        self.build()
        
    def build(self):
        welcome_label = tk.Label(self, text='Welcome to the Chat Client')
        username_label = tk.Label(self, text='Username')
        self.username_entry = tk.Entry(self, text='')
        password_label = tk.Label(self, text='Password')
        self.password_entry = tk.Entry(self, text='', show='*')
        button= tk.Button(self, text='Log In', command=lambda: self.controller.init_connect())

        welcome_label.grid(row=0, column=0, columnspan=2, sticky='nsew')
        username_label.grid(row=1, column=0, sticky='e')
        self.username_entry.grid(row=1, column=1, sticky='e')
        password_label.grid(row=2, column=0, sticky='e')
        self.password_entry.grid(row=2, column=1, sticky='e')
        button.grid(row=3, column=0, columnspan=2, sticky='n')