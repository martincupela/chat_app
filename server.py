import socket
import argparse
import select
import json
import pickle
from queue import Queue, Empty

from baseserver import Server
from mixin import MessageProcessingMixin

def get_first_key_by_value(_dict, value):
    keys = list(filter(lambda k: _dict[k] == value, _dict))
    if keys:
        return keys[0]


class IncorrectCredentials(Exception):
    pass

class MultiConnectionServer(MessageProcessingMixin, Server):

    SOCKET_FAMILY = socket.AF_INET
    SOCKET_TYPE = socket.SOCK_STREAM

    def __init__(self, host, port):
        '''
        :param host: host IP address
        :type host: string
        :param port: port number to bind to
        :type port: int
        '''
        super().__init__(host, port)
        
        self.inputs = [self._listener]
        self.outputs = [] 
        self.messages = {} # {socket: message queue}
        self.username = "server"
        self.users = {self._listener: self.username} # {socket : 'username'}


    def handle(self):
        '''
        Iterate over all the connections and read / write data in
        '''
        read, write, exceptions = select.select(self.inputs, self.outputs, self.inputs)

        for sock in read:
            if sock is self._listener:
                self.accept_connection()
            else:
                self.process_request(sock)
        
        for sock in write:
            self.send_responses(sock)

        for sock in exceptions:
            self.remove(sock)


    def accept_connection(self):
        '''
        Accepts new client connection and registers it among
        inputs and in messages mapping
        ''' 
        connection, address = self._listener.accept()
        connection.setblocking(0)
        self.inputs.append(connection)

        if connection not in self.messages:
            self.messages[connection]= Queue()

    def process_login(self, sock, message):
        sender = message['header']['from']
        
        if sender in self.users.values():
            data = self.create_message('username taken', to=sender)
            sock.sendall(data)
            raise IncorrectCredentials
        else:
            print('{} has connected...'.format(sender))
            self.users[sock] = sender
            self.notify_users_update(sock)


    def notify_users_update(self, newcomer=None):
        users = list(self.users.values())
        users.remove('server')
        connected_users = json.dumps(users)

        for sock, username in self.users.items():
            if sock is not self._listener:
                intro = 'success'  if newcomer and sock == newcomer else 'users'
                
                data = self.create_message(intro + connected_users, to=username)
                self.messages[sock].put(data)
                self.outputs.append(sock)


    def get_response(self, message, sock):
        target_sock = get_first_key_by_value(self.users, message['header']['to'])

        if target_sock in self.messages:
            response = pickle.dumps(message)

            self.messages[target_sock].put(response)
            self.outputs.append(target_sock)


    def process_request(self, sock):
        '''
        Read the data from the socket and store it among received messages.
        Socket just read is moved among those waiting for output.
        '''
        try:
            data = self.recvall(sock)
            messages = self.parse_messages(data)
            print('RECEIVED {} messages'.format(len(messages)))

            for message in messages:
                if message['body'] == 'login' and message['header']['to'] == '':
                    self.process_login(sock, message)
                else:
                    self.get_response(message, sock)
        except (ConnectionError, IncorrectCredentials):
            self.remove(sock)


    def remove(self, sock):
        '''
        Removes traces of dead client connection
        '''
        self.inputs.remove(sock)
        del self.messages[sock]
        if sock in self.outputs:
            self.outputs.remove(sock)
        if sock in self.users:
            print('{} left...'.format(self.users[sock]))
            del self.users[sock]
            self.notify_users_update()
        sock.close()
        
        


    def send_responses(self, sock):
        '''
        Send messages intended for a given socket
        '''
        while True:
            try:
                response = self.messages[sock].get_nowait()
                print('SENDING to {}'.format(self.users[sock]))
            except Empty:
                self.outputs.remove(sock)
                break
            sock.sendall(response)
            

    def recvall(self, client, chunk_size=16):
        data = b''
        try:
            while True:
                received = client.recv(chunk_size)
                if not received: # connection terminated
                    raise ConnectionError
                else:
                    data += received
        except BlockingIOError:
            print('Finished reading...')
        return data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Simple TCP server args')
    parser.add_argument('--host', default="localhost", 
                        help='interface the server listens at (IP:PORT);')
    parser.add_argument('--port', metavar='PORT', type=int, default=9000,
                        help='TCP port (default 9000)')
    
    args = parser.parse_args()
    server = MultiConnectionServer(**vars(args))
    server.serve()