import argparse

from baseserver import Server

class FixedLengthServer(Server):

    def __init__(self, host, port, bytecount=16, num_connections=1):
        '''
        :param host: host IP address
        :type host: string
        :param port: port number to bind to
        :type port: int
        :param bytecount: number of bytes that can be received on one read from client socket
        :type bytecount: int
        :param num_connections: allowed number of waiting client connections
        :type num_connections: int
        '''
        super().__init__(host, port)
        self.num_connections = num_connections
        self.bytecount = bytecount

    def recvall(self, client):
        '''Read the data from the client socket'''
        return client.recv(self.bytecount)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Simple TCP server args')
    parser.add_argument('--host', default="localhost", 
                        help='interface the server listens at (IP:PORT);')
    parser.add_argument('--port', metavar='PORT', type=int, default=9000,
                        help='TCP port (default 9000)')
    parser.add_argument('--bytecount', type=int, default=16,
                        help='Max number of bytes per message (default 16)')
    
    args = parser.parse_args()
    server = FixedLengthServer(**vars(args))
    server.serve()