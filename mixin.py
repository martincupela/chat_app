import pickle
from datetime import datetime as dt
from datetime import timedelta as td

class MessageProcessingMixin:

    MSG_EXPIRES = 3600
    MARK = b'\x80\x03'

    def create_message(self, msg='', to='', sender=''):
        '''
        Create message of given structure.
        '''

        request = {'header': {}}
        request['body'] = msg
        request['header']['to'] = to
        request['header']['from'] = sender or self.username
        request['header']['sent'] = dt.now()
        request['header']['expires'] = request['header']['sent'] + td(seconds=self.MSG_EXPIRES)
        return pickle.dumps(request)

    def parse_messages(self, request):
        '''
        Parse the pickled messages
        '''
        messages = request.lstrip(self.MARK).split(self.MARK)
        messages = [pickle.loads(self.MARK + msg) for msg in messages]
        return messages